<?php

namespace App\Controller;

use App\Entity\Expense;
use App\Form\ExpenseType;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/expense", name="expense_")
 *
 * Class ExpenseController
 * @package App\Controller
 */
class ExpenseController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/create", name="create")
     * @Rest\Get("/{id}/update", name="update")
     *
     * @param Request $request
     * @param int|null $id
     *
     * @return Response
     *
     * @throws Exception
     */
    public function submitAction(Request $request, int $id = null): Response
    {
        $expense = $this->getExpense($id) ?: new Expense();

        $form = $this->createForm(ExpenseType::class, $expense, ['csrf_protection' => false]);
        $form->submit($request->query->all());

        if ($form->isValid()) {
            $expense->setUser($this->getUser());
            $this->getEM()->persist($expense);
            $this->getEM()->flush();

            return $this->json($expense->getId());
        }

        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            /** @var FormError $error */
            if ($error->getOrigin() !== $form) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
        }
        return $this->json($errors);
    }

    /**
     * @Rest\Get("/{id}/delete", name="delete")
     *
     * @param Expense $expense
     *
     * @return Response
     */
    public function deleteAction(Expense $expense): Response
    {
        $this->getEM()->remove($expense);
        $this->getEM()->flush();

        return $this->json('OK');
    }

    /**
     * @param int|null $id
     * @return Expense|null|object
     */
    private function getExpense(int $id = null): ?Expense
    {
        if (is_null($id)) {
            return null;
        }

        return $this->getEM()->getRepository('App:Expense')->find($id);
    }

    /**
     * @return \Doctrine\Persistence\ObjectManager
     */
    private function getEM()
    {
        return $this->getDoctrine()->getManager();
    }
}
