<?php

namespace App\Controller;

use App\Entity\Expense;
use App\Form\ExpenseType;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/statistic", name="statistic_")
 *
 * Class StatisticController
 * @package App\Controller
 */
class StatisticController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/list", name="list")
     * @Rest\QueryParam(name="year", requirements="\d+", strict=true, nullable=false, description="Год")
     * @Rest\QueryParam(name="month", requirements="\d+", strict=true, nullable=false, description="Месяц")
     * @return Response
     */
    public function listAction(ParamFetcherInterface $paramFetcher, EntityManagerInterface $em): Response
    {
        $dateStart = $dateEnd = $paramFetcher->get('year') . '-';
        $month = intval($paramFetcher->get('month'));
        $dateStart .= $month . '-1';
        $dateEnd .= ($month + 1) . '-1';

        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('App:Category')
            ->createQueryBuilder('c');
        $q = $qb->select('c.name, SUM(e.cost) as sum')
            ->innerJoin('c.expenses', 'e')
            ->where('e.date >= :date_start')
            ->andWhere('e.date < :date_end')
            ->setParameters(['date_start' => $dateStart, 'date_end' => $dateEnd])
            ->groupBy('c');

        $results = $q->getQuery()->getArrayResult();
        $sum = array_reduce($results, function ($carry, $item) {
            return $carry + $item['sum'];
        }, 0);
        array_walk($results, function (&$item) use ($sum) {
            $item['percent'] = round($item['sum'] / $sum * 100);
        });

        return $this->json($results);
    }
}
