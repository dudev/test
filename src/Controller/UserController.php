<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @Rest\Get("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function loginAction(Security $security, AuthenticationUtils $authenticationUtils): Response
    {
        if ($security->isGranted('ROLE_USER')) {
            return new JsonResponse('OK');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        return new JsonResponse($error ? $error->getMessage() : 'KO', 403);
    }

    /**
     * @Rest\Get("/", name="check_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function checkAction(Security $security, AuthenticationUtils $authenticationUtils): Response
    {
        return $this->loginAction($security, $authenticationUtils);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logoutAction()
    {
    }
}
