FROM php:7.3-fpm as soft
WORKDIR /app

RUN apt-get update \
    && apt-get install -y \
    libpng-dev \
    librabbitmq-dev \
    libmagickwand-dev

RUN docker-php-ext-install \
    pdo_mysql \
    exif \
    gd \
    bcmath \
    sockets \
    && pecl install imagick \
    && pecl install amqp \
    && docker-php-ext-enable amqp \
    && docker-php-ext-enable imagick


################
FROM soft as backend
RUN apt update && apt install -y git unzip zip libzip-dev && docker-php-ext-install zip
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY . .
#RUN composer install -o
